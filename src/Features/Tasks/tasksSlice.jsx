import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    tasks : [],
    task : {}
}

const taskSlice = createSlice({
    name: 'Task',
    initialState,
    reducers: {
        addTask: (state, action) => {
            state.tasks.push(action.payload)
        },
        editTask: (state, action) => {
            state.task = action.payload
        },
        deleteTask: (state, action) => {
            state.tasks.splice(state.Tasks.findIndex((Task) => Task.id === action.payload), 1);        }
    }
})

export const { addTask, editTask, deleteTask } = taskSlice.actions

export default taskSlice.reducer