import logo from './logo.svg';
import './App.css';
import Linking from './Components/Linking';
import { Routes, Route, Link } from "react-router-dom";
import AddTask from './Components/AddTask';
import Error from './Components/Error';
import ListTask from './Components/ListTask'
import Task from './Components/Task';

function App() {
  return (
    <div className="App">
      <Linking />
      <Routes>
          <Route path="/" element={<AddTask />} />
          <Route path="/tasks" element={<ListTask />} />
          <Route path="/tasks/:id" element={<Task />} />
          <Route path="*" element={<Error />} />
        </Routes>
    </div>
  );
}

export default App;
