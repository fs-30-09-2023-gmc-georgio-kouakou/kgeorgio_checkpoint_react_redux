import ListGroup from 'react-bootstrap/ListGroup';
import { useSelector } from 'react-redux'
import Button from 'react-bootstrap/Button';
import { useDispatch } from 'react-redux';
import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import './ListTask.scss'
import { addTask, deleteTask, editTask } from '../Features/Tasks/tasksSlice';


const ListTask = () => {
    const chooseStyle = ["secondary","danger",,"success","warning","info","light","dark"]
    const randomChooseStyle = Math.floor(Math.random() * chooseStyle.length);
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const tasks = useSelector((state) => state.task.tasks)
    console.log(tasks)

    const handleDelete= (main_id) =>{
        dispatch(deleteTask(main_id));
    }

    return (
        <ListGroup>
            {tasks && tasks.length > 0 && tasks.map((t, key) => (
                <ListGroup.Item action variant={chooseStyle[randomChooseStyle]} key={key} className="mt-1">
                    <div className="category--flex">
                        <div className="category--flex--nom">
                            {t.name_task}
                        </div>


                        <div>
                            <Button variant="warning" onClick={()=> navigate(`/tasks/${t.id}`)}>Voir</Button>
                            <Button variant="success" onClick={()=> navigate(`/${t.id}`)}>Modifier</Button>
                            <Button variant="danger" onClick={()=> handleDelete(t.id)}>Supprimer</Button>
                        </div>
                    </div>
                </ListGroup.Item>
            ))}
        </ListGroup>
    )
}

export default ListTask;