import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { useState } from 'react';
import { useDispatch } from 'react-redux'
import { addTask } from '../Features/Tasks/taskSlice';

function AddTask() {
    const dispatch = useDispatch();
    const [dataForm, setDataForm] = useState({
        name_task: "",
        description_task: "",
        status_task: null,
        id: Date.now()
    })

    const handleSubmit = (e) => {
        e.preventDefault();
        console.log(dataForm)
        dispatch(addTask(dataForm))
    }


    return (
        <Form className="container">
            <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Name of the task</Form.Label>
                <Form.Control type="email" placeholder="Enter the name of the task" value={dataForm.name_task} onChange={(e) => (setDataForm({ ...dataForm, name_task: e.target.value }))} />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label>Description of the task</Form.Label>
                <Form.Control as="textarea" rows={3} value={dataForm.description_task} onChange={(e) => (setDataForm({ ...dataForm, description_task: e.target.value }))}/>
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicCheckbox">
                <Form.Check type="checkbox" label="It's done or not ?" value={dataForm.status_task} onChange={(e) => (setDataForm({ ...dataForm, status_task: e.target.value }))} />
            </Form.Group>
            <Button variant="primary" type="submit" onClick={handleSubmit}>
                Submit
            </Button>
        </Form>
    );
}

export default AddTask;