import { useSelector } from 'react-redux';
import { useParams, useNavigate } from 'react-router-dom';
import { useState } from 'react';
import { useDispatch } from 'react-redux'


const Task = () =>{
    const tasks = useSelector((state) => state.task.tasks)
    const {id} = useParams()
    const navigate = useNavigate()

    const find_tasks = tasks.find(t=> t.id===id)
    console.log("find tasks",find_tasks)

    return(
        <div>
            <h3>Name of Task :</h3>
            <p>{find_tasks.name_task}</p>

            <h3>Description of Task</h3>
            <p>{find_tasks.description_task}</p>

            <h3>Status of Task</h3>
            <p>{find_tasks.status_task}</p>
        </div>
    )
}

export default Task